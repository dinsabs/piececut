Setup:
===

```bash
$ ./manage.py migrate
$ pip install -r requirements.txt
$ python3 db_populate.py
$ ./manage.py runserver 0:8000
```

Example:
===
### request:

```
POST /search/ HTTP/1.1
Host: 127.0.0.1:8000
Content-Type: application/json
```
```json
{
	"pieces": ["ша", "ар"],
	"cutouts": ["бу", "ки"]
}
```
### response:
```json
[
    "3. Однако сегодня мы слишком часто копируем сделанное до нас.
    Книга «От нуля к единице» о том, как создавать компании..."
]
```