from django.contrib import admin

from .models import BookText


admin.site.register(BookText)
