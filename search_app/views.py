from functools import reduce

from rest_framework.views import APIView, Response

from .models import BookText


class IndexView(APIView):

    def post(self, request):

        pieces = request.data.get('pieces')
        cutouts = request.data.get('cutouts')
        response = []

        query = BookText.get_generated_query(pieces)
        result = BookText.objects.raw(query)

        for obj in result:
            splitted = obj.text.split(' ')
            filtered = [word for word in splitted if self.check(word, cutouts)]  # filtered list with cutouts
            response.append(reduce(lambda x, y: x + ' ' + y, filtered))  # joining text

        return Response(response)

    @staticmethod
    def check(word, cuts):
        """
        checks whether word contains cutouts or not
        """
        for cut in cuts:
            if cut in word:
                return False
        return True
