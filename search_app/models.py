from django.db import models


class BookText(models.Model):
    text = models.TextField(db_index=True)

    @classmethod
    def get_generated_query(self, pieces):
        """
        generates single query for retrieving words containing `pieces`
        """
        piece = pieces.pop(0)
        query = f'SELECT * FROM search_app_booktext WHERE text LIKE \'%{piece}%\''
        for piece in pieces:  # chaining query for retrieving all matches from pieces
            chainer = f' AND text LIKE \'%{piece}%\''
            query += chainer
        return query


    def __str__(self):
        return self.text[:30] + ' . . .'
